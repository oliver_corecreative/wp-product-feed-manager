<?php

/**
 * @package WP Product Feed Manager/Application/Functions
 * @version 2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }

function channel_file_text_data( $channel_id ) {
	switch ( $channel_id ) {
		case '1': // google
			$category_name = 'google_product_category';
			$description_name = 'description';
			break;
		
		case '2': // bing
			$category_name = 'BingCategory';
			$description_name = 'Description';
			break;
		
		case '3': // beslis
			$category_name = 'Categorie';
			$description_name = 'Omschrijving';
			break;

		case '4': // pricegrabber
			$category_name = 'Categorization';
			$description_name = 'Detailed Description';
			break;

		case '5': // shopping
			$category_name = 'Category';
			$description_name = 'Product_Description';
			break;

		case '6': // amazon
			$category_name = 'Category';
			$description_name = 'Description';
			break;

		case '7': // connexity
			$category_name = 'Category';
			$description_name = 'Description';
			break;

		case '9': // nextag
			$category_name = 'Category';
			$description_name = 'Description';
			break;

		case '10': // kieskeurig
			$category_name = 'productgroep';
			$description_name = 'productomschrijving';
			break;

		case '11': // vergelijk
			$category_name = 'Category';
			$description_name = 'ProductDescription';
			break;

		case '12': // koopjespakker
			$category_name = 'category';
			$description_name = 'description';
			break;

		case '13': // avantlink
			$category_name = 'google_product_category';
			$description_name = 'description';
			break;

		case '14': // zbozi
			$category_name = 'CATEGORYTEXT';
			$description_name = 'DESCRIPTION';
			break;
		
		case '15': // comcon
			$category_name = '';
			$description_name = '';
			break;
		
		case '16': // facebook
			$category_name = 'google_product_category';
			$description_name = 'description';
			break;
		
		case '17': // Bol.com
			$category_name = '';
			$description_name = '';
			break;
		
		case '18': // Adtraction
			$category_name = '';
			$description_name = 'description';
			break;
		
		case '19': // Ricardo
			$category_name = 'CategoryNr';
			$description_name = 'Descriptions[0].ProductDescription';
			break;
		
		case '20': // eBay
			$category_name = 'condition';
			$description_name = 'productDescription';
			break;
		
		case '21': // Shopzilla
			$category_name = 'Category ID';
			$description_name = 'Description';
			break;
		
		case '22': // Converto
			$category_name = '';
			$description_name = 'description';
			break;
		
		case '23': // Idealo
			$category_name = '';
			$description_name = 'Produktbeschreibung';
			break;

		case '24': // Heureka
			$category_name = 'CATEGORYTEXT';
			$description_name = 'DESCRIPTION';
			break;

		case '25': // Pepperjam
			$category_name = 'category_network';
			$description_name = 'description_long';
			break;
		
		case '996': // marketingrobot TSV
		case '997': // marketingrobot TXT
		case '998': // marketingrobot CSV
		case '999': // marketingrobot
			$category_name = 'product_category';
			$description_name = 'description';
			break;

		default:
			$category_name = 'google_product_category';
			$description_name = 'description';
			break;
	}

	return array( 'channel_id' => $channel_id, 'category_name' => $category_name, 'description_name' => $description_name );
}

/**
 * returns the type of file the channel outputs
 * 
 * @param string $channel_id
 * @return string
 */
function get_file_type( $channel_id ) {
	switch ( $channel_id ) {
		case '2': // bing
		case '4': // pricegrabber
		case '6': // amazon
		case '7': // connexity
		case '9': // nextag
		case '12': // koopjespakker
		case '21': // shopzilla
		case '25': // pepperjam
		case '997': // Custom TXT Feed
			return 'txt';
			
		case '15': // comcon
		case '17': // bol.com
		case '19': // Ricardo.ch
		case '22': // Converto
		case '23': // Idealo
		case '998': // Custom CSV Feed
			return 'csv';
			
		case '996': // Custom TSV Feed
			return 'tsv';
			
		default:
			return 'xml';
	}
}

/**
 * some channels use different csv separators. Only required for csv channels
 * 
 * @param string $channel_id
 * @return string
 */
function get_correct_csv_separator( $channel_id ) {
	switch ( $channel_id ) {
		case '15': // comcon
			return ';';
			
		default:
			return ',';
	}
}

/**
 * and other channels use a different separator for the header line
 * 
 * @param string $channel_id
 * @return string
 */
function get_correct_csv_header_separator( $channel_id ) {
	switch ( $channel_id ) {
		case '22': // Converto
			return '|';
			
		default:
			return get_correct_csv_separator( $channel_id );
	}
}

/**
 * returns true if the channel uses the categories from the webshop
 * 
 * @param string $channel_id
 * @return boolean
 */
function channel_uses_category( $channel_id ) {
	switch ( $channel_id ) {
		case '15': // comcon
		case '17': // bol.com
		case '22': // converto
		case '23': // idealo
		case '25': // pepperjam
			return false;
			
		default:
			return true;
	}
}

/**
 * returns the text that has to be used as node name for every product in the xml file
 * 
 * @param string $channel
 * @return string
 */
function product_node_name( $channel ) {
	switch ( $channel ) {
		case '1':
		case '13':
		case '16':
			return 'item';

		case '5':
		case '11':
		case '20':
			return 'Product';

		case '14':
		case '24':
			return 'SHOPITEM';

		default:
			return 'product';
	}
}

/**
 * returns a pre-tag when the channel requires it
 * 
 * @param string $channel
 * @return string
 */
function get_node_pretag( $channel ) {
	switch ( $channel ) {
		case '1': // google
		case '16': // facebook
			return 'g:';
		
		default:
			return '';
	}
}

/**
 * returns true if a channel is a custom channel
 * 
 * @param string $channel
 * @return boolean
 */
function channel_is_custom_channel( $channel ) {
	switch( $channel ) {
		case '996': // Custom TSV Feed
		case '997': // Custom TXT Feed
		case '998': // Custom CSV Feed
		case '999': // Custom XML Feed
		case 'marketingrobot_tsv': // Custom TSV Feed
		case 'marketingrobot_txt': // Custom TXT Feed
		case 'marketingrobot_csv': // Custom CSV Feed
		case 'marketingrobot': // Custom XML Feed
			return true;
			
		default:
			return false;
	}
}

/**
 * Some channels have items that can be placed more than once in a feed. This function identifies these items
 * 
 * @since 1.9.0
 * 
 * @param string $channel
 * @param string $key
 * @return boolean true if the item can be placed more than once in a feed
 */
function multi_item_rows_allowed( $channel, $key ) {
	switch( $channel ) {
		case '1': // google
			return $key === 'display_ads_similar_id' 
				|| $key === 'excluded_destination' 
				|| $key === 'shipping' 
				|| $key === 'adwords_labels' ? true : false;
			break;
			
		case '14': // Zbozi
		case '24': // Heureka
			return $key === 'PARAM' ? true : false;
			break;
			
		default:
			return false;
	}
}

/**
 * checks if the current plugin version supports the selected channel
 * 
 * @since 1.8.0
 * 
 * @param string $channel
 * @return boolean
 */
function plugin_version_supports_channel( $channel ) {
	$supported_channels = array(
		'google',
		'bing',
		'beslis',
		'pricegrabber',
		'shopping',
		'amazon',
		'connexity',
		'nextag',
		'kieskeurig',
		'vergelijk',
		'koopjespakker',
		'avantlink',
		'zbozi',
		'comcon',
		'facebook',
		'marketingrobot_tsv',
		'marketingrobot_txt',
		'marketingrobot_csv',
		'marketingrobot',
		'bol',
		'adtraction',
		'ricardo',
		'ebay',
		'shopzilla',
		'converto',
		'idealo',
		'heureka',
		'pepperjam'
	);

	return in_array( $channel, $supported_channels );
}

/**
 * returns the channel specific class-feed.php class name
 * 
 * @param string $channel_id
 * @return string
 */
function get_correct_channel_class_name( $channel_id ) {
	if( ! $channel_id ) { 
		wppfm_write_log_file( 'Error 3821 - Could not identify the selected channel id' );
		return false;
	}
	
	$channel_base_class	 = new WPPFM_Channel();
	$channel_short_name	 = $channel_base_class->get_channel_short_name( $channel_id );
	
	$channel_class = 'WPPFM_' . ucfirst( $channel_short_name ) . '_Feed_Class';

	if( ! class_exists( $channel_class ) ) {
		if ( file_exists( WPPFM_CHANNEL_DATA_DIR . '/' . $channel_short_name . '/class-feed.php' ) ) {
			require_once ( WPPFM_CHANNEL_DATA_DIR . '/' . $channel_short_name . '/class-feed.php' );
		}
	}
	
	return $channel_class;
}