/*global wppfm_feed_settings_form_vars */
var _feedHolder;

function wppfm_makeFeedFilterWrapper( feedId, filter ) {
	var	htmlCode = wppfm_feed_settings_form_vars.all_products_included;
	var displayAccept = 'none';
	var displayEdit = 'none';
	
	if ( filter && filter.constructor === Array ) {
		var filterObject = JSON.parse(filter[0]['meta_value']);
		var nrFilters = wppfm_countObjectItems( filterObject );
		var i = 1;
		displayAccept = 'initial';
		displayEdit = 'none';
		
		for ( var key in filterObject ) {
			var filterArray = filterObject[key][i].split( '#' );
			var last = nrFilters === i ? true : false;
			htmlCode += '<span id="filter-accept-text"  style="display:' + displayAccept + ';"> ' + wppfm_feed_settings_form_vars.all_products_except + ':</span>';
				
			htmlCode += wppfm_showEditFeedFilter( feedId, i, filterArray, last );
			i++;
		}
	} else {
		displayAccept = 'none';
		displayEdit = 'initial';
		htmlCode += '<span id="filter-accept-text"  style="display:' + displayAccept + ';"> ' + wppfm_feed_settings_form_vars.all_products_except + ':</span>';
	}

	htmlCode += '<span id="filter-edit-text" style="display:' + displayEdit + ';"> (<a class="edit-feed-filter wppfm-btn wppfm-btn-small" href="javascript:void(0)" id="edit-feed-filters-' + feedId;
	htmlCode += '" onclick="wppfm_editFeedFilter(' + feedId + ')">' + wppfm_feed_settings_form_vars.edit + '</a>)</span>';
	
	jQuery( '.product-filter-condition-wrapper' ).html( htmlCode );
	jQuery( '.main-product-filter-wrapper' ).show();
}

function wppfm_showEditFeedFilter( feedId, filterLevel, filterArray, last ) {
	var htmlCode = '<div class="filter-wrapper" id="filter-wrapper-' + feedId + '-' + filterLevel + '">';
	
	htmlCode += wppfm_filterPreCntrl( feedId, filterLevel, filterArray[0] );
	htmlCode += wppfm_filterSourceCntrl( feedId, filterLevel, filterArray[1] );
	htmlCode += wppfm_filterOptionsCntrl( feedId, filterLevel, filterArray[2] );
	htmlCode += wppfm_filterInputCntrl( feedId, filterLevel, 1, filterArray[3] );
	htmlCode += wppfm_filterInputCntrl( feedId, filterLevel, 2, filterArray[3] );
	htmlCode += ' (<a class="feed-filter-remove wppfm-btn wppfm-btn-small" href="javascript:void(0)" id="filter-remove-';
	htmlCode += filterLevel + '" onclick="wppfm_removeFilter(' + feedId + ', ' + filterLevel + ')">' + wppfm_feed_settings_form_vars.remove + '</a>)';
	htmlCode += last ? '<span id="add-feed-filter-link"> (<a class="feed-filter-add wppfm-btn wppfm-btn-small" href="javascript:void(0)" id="filter-add-' 
		+ filterLevel + '" onclick="wppfm_addFilter(' + feedId + ', ' + filterLevel + ')">' + wppfm_feed_settings_form_vars.add + '</a>)</span>' : '';
	
	htmlCode += '</div>';

	return htmlCode;
}

function wppfm_filterChanged( feedId, filterLevel ) {
	var identString = feedId + '-' + filterLevel;
	var optionsValue = jQuery( '#filter-options-control-' + identString ).val();
	var inputValue = '';

	// display the correct input fields
	if ( optionsValue === '4' || optionsValue === '5' || optionsValue === '14' ) {
		if ( optionsValue === '14' ) {
			jQuery( '#filter-input-span-' + identString + '-2' ).show();
			jQuery( '#filter-input-span-' + identString + '-1' ).show();
			inputValue = jQuery( '#filter-input-control-' + identString + '-1' ).val() 
				+ '#0#' + jQuery( '#filter-input-control-' + identString + '-2' ).val();
		} else {
			jQuery( '#filter-input-span-' + identString + '-2' ).hide();
			jQuery( '#filter-input-span-' + identString + '-1' ).hide();
		}
		
	} else {
		jQuery( '#filter-input-span-' + identString + '-2' ).hide();
		jQuery( '#filter-input-span-' + identString + '-1' ).show();
		inputValue = jQuery( '#filter-input-control-' + identString + '-1' ).val();
	}

	if( wppfm_feedFilterIsFilled( feedId, filterLevel ) ) {
		var preValue = filterLevel > 1 ? jQuery( '#filter-pre-control-' + identString ).val() : '0';
		var sourceValue = jQuery( '#filter-source-control-' + identString ).val();

		var newValues = [ preValue, sourceValue, optionsValue, inputValue ];

		_feedHolder.setFeedFilter( changeFeedFilterValue( _feedHolder['feedFilter'], newValues, filterLevel ) );
	}
}

function wppfm_addFilter( feedId, filterLevel ) {
	if ( wppfm_feedFilterIsFilled( feedId, filterLevel ) || filterLevel === 0 ) {
		jQuery( '#add-feed-filter-link' ).remove();
		jQuery( '.product-filter-condition-wrapper' ).append( wppfm_showEditFeedFilter( feedId, filterLevel + 1, '', true ) );
		jQuery( '#filter-edit-text' ).hide();
		jQuery( '#filter-accept-text' ).show();
	} else {
		alert( wppfm_feed_settings_form_vars.fill_filter_warning );
	}
}

function wppfm_removeFilter( feedId, filterLevel ) {
	_feedHolder.removeFeedFilter( filterLevel );
	wppfm_makeFeedFilterWrapper( feedId, _feedHolder['feedFilter'] );
}

function wppfm_editFeedFilter( feedId ) {
	wppfm_addFilter( feedId, 0 );
}