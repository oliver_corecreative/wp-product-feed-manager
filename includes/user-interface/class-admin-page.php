<?php

/**
 * WP Product Feed Manager Admin Page Class.
 *
 * @package WP Product Feed Manager/User Interface/Classes
 * @version 1.1.0
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! class_exists( 'WPPFM_Admin_Page' ) ) :

	/**
	 *  WPPFM Admin Page Class
	 */
	class WPPFM_Admin_Page {
	
		function __construct() { }

		/**
		 * Returns a string containing the standard header for an admin page.
		 * 
		 * @return string
		 */
		protected function admin_page_header( $header_text = "WP Product Feed Manager" ) {
			$spinner_gif = WPPFM_PLUGIN_URL . '/images/ajax-loader.gif';
			$ticket_link = WPPFM_EDD_SL_ITEM_NAME === 'WP Product Feed Manager' ? 'https://wordpress.org/support/plugin/wp-product-feed-manager' 
				: WPPFM_EDD_SL_STORE_URL . '/support/';
			
			return
			'<div class="wrap">
			<div class="feed-spinner" id="feed-spinner" style="display:none;">
				<img id="img-spinner" src="' . $spinner_gif . '" alt="Loading" />
			</div>
			<div class="data" id="wp-product-feed-manager-data" style="display:none;"><div id="wp-plugin-url">' . WPPFM_UPLOADS_URL . '</div></div>
			<div class="main-wrapper header-wrapper" id="header-wrapper">
			<div class="header-text"><h1>' . $header_text . '</h1></div>
			<div class="sub-header-text"><h3>' . esc_html__( 'Manage your feeds with ease', 'wp-product-feed-manager' ) . '</h3></div>
			<div class="links-wrapper" id="header-links"><a href="' . WPPFM_EDD_SL_STORE_URL . '/support/documentation/create-product-feed/" target="_blank">'
			. esc_html__( 'Click here for the documentation', 'wp-product-feed-manager' ) . '</a></div>
			<div class="links-wrapper" id="ticket-link"><a href="' . $ticket_link . '" target="_blank">' . esc_html__( 'Something not working? Click here for support', 'wp-product-feed-manager' ) . '</a></div>
			</div>';
		}
		
		protected function feedback_cta() {
			return 	'<div class="wppfm-feedback"><a href="https://wordpress.org/support/plugin/wp-product-feed-manager/reviews#new-post" target="_blank">'
			. esc_html__( 'Like Our Plugin? We\'d Love Your Feedback', 'wp-product-feed-manager' ) . ' ★★★★★</a></div>';
		}

		/**
		 * Returns a string containing the standard footer for an admin page.
		 * 
		 * @return string
		 */
		protected function admin_page_footer() {
			return
			'<div class="main-wrapper footer-wrapper" id="footer-wrapper">
			 <div class="links-wrapper" id="footer-links"><a href="' . WPPFM_EDD_SL_STORE_URL . '" target="_blank">' . esc_html__( 'About Us', 'wp-product-feed-manager' ) . '</a> 
			 | <a href="' . WPPFM_EDD_SL_STORE_URL . '/support/" target="_blank">' . esc_html__( 'Contact Us', 'wp-product-feed-manager' ) . '</a> 
			 | <a href="' . WPPFM_EDD_SL_STORE_URL . '/terms/" target="_blank">' . esc_html__( 'Terms and Conditions', 'wp-product-feed-manager' ) . '</a>
			 | <a href="' . WPPFM_EDD_SL_STORE_URL . '/support/documentation/create-product-feed/" target="_blank">' . esc_html__( 'Documentation', 'wp-product-feed-manager' ) . '</a></div>
			 </div></div>';
		}

		protected function message_field( $alert = '' ) {
			$display_alert = empty( $alert ) ? 'none' : 'block';
		
			return
			'<div class="message-field notice notice-error" id="error-message" style="display:none;"></div>
			 <div class="message-field notice notice-success" id="success-message" style="display:none;"></div>
			 <div class="message-field notice notice-warning" id="disposible-warning-message" style="display:' . $display_alert . ';"><p>' . $alert . '</p>
			<button type="button" id="disposible-notice-button" class="notice-dismiss"></button>
			</div>';
		}

		// ref HWOTBERH
		protected function control_main_field( $stat )  {
			$license = get_option( 'wppfm_lic_key' );
			$license_exp = $stat === 'expired' ? '<p>' . __( 'Your key seems to be expired. If you are sure you have a valid key, please open a ticket at wpmarketingrobot.com/support/', 'wp-product-feed-manager' ) . '</p>' : '';

			$html = '<div class="edd-wrap notice-success notice below-h2"><h2>' . __( 'Plugin License Options', 'wp-product-feed-manager' ) . '</h2>';
			$html .= '<form method="post">';
			$html .= settings_fields( 'wppfm_lic_group' );
			$html .= '<table class="form-table">';
			$html .= '<tbody><tr valign="top">';
			$html .= '<th scope="row" valign="top">';
			$html .= esc_html__( 'License Key', 'wp-product-feed-manager' );
			$html .= '</th><td><input id="wppfm_license_key" name="wppfm_license_key" type="text" class="regular-text"';
			$html .= ' value="' . esc_attr__( $license ) . '" />';
			$html .= $license_exp . '</td></tr>';
			$html .= '<tr valign="top"><th scope="row" valign="top">&nbsp;</th>';
			$html .= '<td><span>Click <a href="//www.wpmarketingrobot.com/terms/" target="_blank">here to read our Terms and Conditions</a> before using WP Product Feed Manager.</span>';
			$html .= '</td></tr>';
			$html .= '<tr valign="top"><th scope="row" valign="top">';
			$html .= esc_html__( 'I accept your Terms and Conditions', 'wp-product-feed-manager' );
			$html .= '</th><td><input id="wppfm_accept_eula" name="user_accepts_eula" type="checkbox" />';
			$html .= '</td></tr>';
			$html .= '<tr valign="top"><th scope="row" valign="top">';
			$html .= esc_html__( 'Activate License', 'wp-product-feed-manager' );
			$html .= '</th><td>';
			$html .= wp_nonce_field( 'wppfm_lic_nonce', 'wppfm_lic_nonce' );
			$html .= '<input type="submit" class="button-secondary" name="wppfm_license_activate" value="';
			$html .= esc_html__( 'Activate License', 'wp-product-feed-manager' ) . '" id="wppfm_license_activate" disabled />';
			$html .= '</td></tr>';
			$html .= '</tbody></table>';
			$html .= '</form></div>';

			return $html;
		}

	}

	

     // end of WPPFM_Admin_Page class

endif;