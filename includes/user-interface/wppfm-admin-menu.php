<?php

/**
 * @package WP Product Feed Manager/User Interface/Functions
 * @version 1.1.0
 */

if ( ! defined('ABSPATH') ) { exit; }

/**
 * Add the feed manager menu in the Admin page
 */
function wppfm_add_feed_manager_menu( $channel_updated = false ) {
	// defines the feed manager menu
	add_menu_page(
		__( 'WP Feed Manager', 'wp-product-feed-manager' ), 
		__( 'Feed Manager', 'wp-product-feed-manager' ), 
		'manage_woocommerce', 'wp-product-feed-manager', 
		'wppfm_main_admin_page', 
		esc_url( WPPFM_PLUGIN_URL . '/images/app-rss-plus-xml-icon.png' ) 
	);

	add_submenu_page(
		'wp-product-feed-manager',
		__( 'Add Feed', 'wp-product-feed-manager' ), 
		__( 'Add Feed', 'wp-product-feed-manager' ), 
		'manage_woocommerce', 
		'wp-product-feed-manager-add-new-feed', 
		'wppfm_add_feed_page' 
	);

	// add the channels page only for the full version ref MKFRPLGN
	if ( WPPFM_EDD_SL_ITEM_NAME === 'Woocommerce Product Feed Manager' ) {
		$channel_class = new WPPFM_Channel();
		$channels_updates_counter = (int) $channel_class->get_number_of_updates_from_server( $channel_updated );

		/* translators: %d: Number of channel updates available */
		$counter_title = sprintf( _n( 
				'%d channel update', 
				'%d channel updates', 
				$channels_updates_counter, 
				'wp-product-feed-manager' ), 
			$channels_updates_counter );
		
		/* translators: %d: Number of channel updates available */
		$menu_label = sprintf( __( 'Manage Channels %s', 'wp-product-feed-manager' ), 
			"<span class='update-plugins count-$channels_updates_counter' data-counter='$channels_updates_counter' 
				id='channel-update-counter' title='$counter_title'><span 
					class='wppfm-update-counter'>" . $channels_updates_counter . "</span></span>" );

		add_submenu_page(
			'wp-product-feed-manager', 
			__( 'Manage Channels', 'wp-product-feed-manager' ), 
			$menu_label, 
			'manage_woocommerce', 
			'wp-product-feed-manager-manage-channels', 
			'wppfm_manage_channels_page' 
		);
	}
	
	// add the settings 
	add_submenu_page(
		'wp-product-feed-manager', 
		__( 'Settings', 'wp-product-feed-manager' ),  
		__( 'Settings', 'wp-product-feed-manager' ), 
		'manage_woocommerce', 
		'wppfm-options-page', 
		'wppfm_options_page' 
	);
}

add_action( 'admin_menu', 'wppfm_add_feed_manager_menu' );

/**
 * Adds links to the started guide and premium site
 * 
 * @since 2.6.0
 * 
 * @param array $actions associative array of action names to anchor tags
 * @param string $plugin_file plugin file name
 * @param array $plugin_data array of plugin data from the plugin file
 * @param string $context plugin status context
 * @return string
 */
function wppfm_plugins_action_links( $actions, $plugin_file, $plugin_data, $context ) {
	$actions[ 'starter_guide' ] = '<a href="' . WPPFM_EDD_SL_STORE_URL . '/support/documentation" target="_blank">' . __( 'Starter Guide', 'wp-product-feed-manager' ) . '</a>';
	
	if( 'WP Product Feed Manager' === WPPFM_EDD_SL_ITEM_NAME ) {
		$actions[ 'go_premium' ] = '<a style="color:green;" href="' . WPPFM_EDD_SL_STORE_URL . '" target="_blank"><b>' . __( 'Go Premium', 'wp-product-feed-manager' ) . '</b></a>';
	} else {
		$actions[ 'support' ] = '<a href="' . WPPFM_EDD_SL_STORE_URL . '/support" target="_blank">' . __( 'Get Support', 'wp-product-feed-manager' ) . '</a>';
	}
	
	return $actions;
}

add_filter( 'plugin_action_links_' . WPPFM_PLUGIN_CONSTRUCTOR, 'wppfm_plugins_action_links', 10, 4 );

/**
 * starts the main admin page
 */
function wppfm_main_admin_page() {
	$start = new WPPFM_Main_Admin_Page ();
	$stat = wppfm_generate_current_main_address();

	if( 'valid' === $stat ) { // ref HWOTBERH
		// now let's get things going
		$start->show();
	} else {
		$start->show_main_control_page( $stat );
	}
}

function wppfm_add_feed_page() {
	$stat = wppfm_generate_current_main_address();

	if( 'valid' === $stat ) { // ref HWOTBERH
		$add_new_feed_page = new WPPFM_Add_Feed_Page ();
		$add_new_feed_page->show();
	} else {
		$start = new WPPFM_Main_Admin_Page ();
		$start->show_main_control_page( $stat );
	}
}

/**
 * options page
 */
function wppfm_options_page() {
	$stat = wppfm_generate_current_main_address();

	if( 'valid' === $stat ) { // ref HWOTBERH
		$add_options_page = new WPPFM_Add_Options_Page ();
		$add_options_page->show();
	} else {
		$start = new WPPFM_Main_Admin_Page ();
		$start->show_main_control_page( $stat );
	}
}

// ref MKFRPLGN
function wppfm_manage_channels_page() {
	$stat = wppfm_generate_current_main_address();

	if( 'valid' === $stat ) { // ref HWOTBERH
		$manage_channels_page = new WPPFM_Manage_Channels_Page ();

		$updated = '';

		if ( isset( $_GET['wppfm_action'] ) && isset( $_GET['wppfm_channel'] ) && isset( $_GET['wppfm_nonce']) ) {
			$channel_class = new WPPFM_Channel ();

			$action = $_GET['wppfm_action'];
			$channel = $_GET['wppfm_channel'];
			$nonce = $_GET['wppfm_nonce'];

			switch ( $action ) {
				case 'remove':
					$channel_class->remove_channel( $channel, $nonce );
					break;

				case 'update':
					$channel_class->update_channel( $channel, $_GET['wppfm_code'], $nonce );
					$updated = $channel;
					echo "<script>
						function wppfm_decrease_channel_updates_counter() {
							var oldValue = jQuery( '.wppfm-update-counter' ).text();
							var oldClassName = 'count-'+oldValue;
							var newValue = oldValue > 1 ? oldValue - 1 : '0';
							var newClassName = 'count-'+newValue;
							var element = document.getElementById( 'channel-update-counter' ); 
							jQuery( '.wppfm-update-counter' ).html( newValue );
							element.classList.remove( oldClassName );
							element.classList.add( newClassName );
						}
					</script>";
					echo '<script type="text/javascript">wppfm_decrease_channel_updates_counter();</script>';
					break;

				case 'install':
					$channel_class->install_channel( $channel, $_GET['wppfm_code'], $nonce );
					break;

				default:
					break; // do not react on a wrong action call
			}
		}

		$manage_channels_page->show( $updated );
	} else {
		$start = new WPPFM_Main_Admin_Page ();
		$start->show_main_control_page( $stat );
	}
}

/**
 * Checks if the backups are valid for the current database version and warns the user if not
 * 
 * @since 1.9.6
 */
function wppfm_check_backups() {
	if( ! wppfm_check_backup_status() ) {
		$msg = __( 'Due to the latest update your Feed Manager backups are no longer valid! Please open the Feed Manager Settings page, remove all your backups in and make a new one.', 'wp-product-feed-manager' )
			?><div class="notice notice-warning is-dismissible">
			<p><?php echo $msg; ?></p>
		</div><?php
	}
}

add_action( 'admin_notices', 'wppfm_check_backups' );

// ref HWOTBERH
function wppfm_generate_current_main_address() {
	// do not validate on other wp-admin pages than the own plugins pages
	if ( ! wppfm_on_any_own_plugin_page() ) { return; }

	if ( 'valid' === get_option( 'wppfm_lic_status' ) ) {
		if ( date( 'Ymd' ) === get_option( 'wppfm_lic_status_date' ) ) {
			return 'valid';
		} else {
			return wppfm_edd_status();
		}
	} else {
		return wppfm_edd_status();
	}
}

function wppfm_edd_status() {
	$edd_status = wppfm_check_license( get_option( 'wppfm_lic_key' ) );

	update_option( 'wppfm_lic_status', $edd_status );

	if ( $edd_status === 'valid' ) {
		update_option( 'wppfm_lic_status_date', date( 'Ymd' ) );
		return 'valid';
	} else {
		return $edd_status;
	}
}

function wppfm_register_option() {
	// creates our settings in the options table
	register_setting('wppfm_lic_group', 'wppfm_lic_key', 'wppfm_sanitize_license' );
}

add_action( 'admin_init', 'wppfm_register_option' );

function wppfm_sanitize_license( $new ) {
	$old = get_option( 'wppfm_lic_key' );

	if( $old && $old != $new ) {
		delete_option( 'wppfm_lic_status' ); // new license has been entered, so must reactivate
	}
	
	return $new;
}

function wppfm_activate_license() {
	// listen for our activate button to be clicked
	if( isset( $_POST['wppfm_license_activate'] ) ) {

		// run a quick security check 
	 	if( !check_admin_referer( 'wppfm_lic_nonce', 'wppfm_lic_nonce' ) ) {
			return false; // get out if we didn't click the Activate button
		}

		// retrieve the license from the form
		$license = trim( $_POST[ 'wppfm_license_key'] );
			
		// data to send in our API request
		$api_params = array( 
			'edd_action'=> 'activate_license', 
			'license'   => $license, 
			'item_name' => urlencode( WPPFM_EDD_SL_ITEM_NAME ), // the name of our product in EDD
			'url'       => home_url()
		);
		
		// Call the custom API.
		$response = wp_remote_post( WPPFM_EDD_SL_STORE_URL, array(
			'timeout'   => 5,
			'sslverify' => false,
			'body'      => $api_params
		) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) ) { 
			/*translators: %s: link to the support page */
			echo wppfm_handle_wp_errors_response( $response, sprintf( esc_html__( '2121 - Activating your license failed. Please open a support ticket at %s for support on this issue.', 'wp-product-feed-manager' ), WPPFM_SUPPORT_PAGE_URL ) );
			return false; 
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		
		if ( $license_data ) {
			// $license_data->license will be either "active" or "inactive"
			update_option( 'wppfm_lic_status', $license_data->license );
			update_option( 'wppfm_lic_key', $license );
			update_option( 'wppfm_lic_expires', $license_data->expires );
			update_option( 'wppfm_lic_status_date', date( 'Ymd' ) );
		} elseif ( strpos( $response['body'], 'Fatal error' ) ) {
			/* translators: %s: Error message from wp_remote_posts response */
			echo wppfm_show_wp_error( sprintf( esc_html__( 'An error has occured. It is possible that the wpmarketingrobot website is down. 
				Please check if wpmarketingrobot.com is still active. If not, or if this error consist, please contact
				auke@wpmarketingrobot.com. Error Message: %s' , 'wp-product-feed-manager'), $response['body'] ) );
		}
	}
}

add_action( 'admin_init', 'wppfm_activate_license' );

/**
 * Shows a message on all wp admin pages when the license needs to be renewed
 * 
 * @since 1.9.5
 */
function wppfm_check_license_expiration() {
	$lic_key = get_option( 'wppfm_lic_key' );
	$expires = get_option( 'wppfm_lic_expires' );
	
	if ( 'lifetime' === $expires ) { return; }
	
	if ( ! $lic_key || ! $expires ) { return; }
	
	$today = date( 'Y-m-d H:i:s', current_time( 'timestamp' ) );
	$one_week_date = date_add( date_create( $today ), date_interval_create_from_date_string( '1 week') );
	$almost_expires_date = date_format( $one_week_date, 'Y-m-d H:i:s' );
	$plugin_name = get_plugin_data( WPPFM_PLUGIN_DIR.'wp-product-feed-manager.php' );
	$msg = '';
	$is_dismissible = 'is-dismissible';
	
	if ( $expires < $almost_expires_date ) { 
		wppfm_check_license( $lic_key ); // make sure the status is correct
		$expires = get_option( 'wppfm_lic_expires' );
		$almost_expires_date = date_format( $one_week_date, 'Y-m-d H:i:s' );
	}

	if( $expires <= $today ) {
		/* translators: 1: Name of the plugin, 2: url to the wpmarketingrobot store, 3: license key */
		$msg = sprintf( esc_html__( 'You have an invalid or expired license key for your %1$s plugin. 
			Please <a href="%2$scheckout/?edd_license_key=%3$s&utm_source=client-admin&utm_medium=renewal_link&utm_campaign=license_expired">
			click here to go to the Licenses renewal page</a> to keep the plugin working and your product feeds valid.', 
			'wp-product-feed-manager' ), 
			$plugin_name[ 'Name' ], 
			WPPFM_EDD_SL_STORE_URL, 
			$lic_key );
		$msg_id = 'wppfm-licence-expired';
		$msg_type = 'error';
		$is_dismissible = '';
	} elseif ( $expires < $almost_expires_date  ) {
		/* translators: 1: Name of the plugin, 2: url to the wpmarketingrobot store, 3: license key */
		$msg = ! get_option( 'wppfm_license_notice_surpressed' ) ? sprintf( esc_html__( 'The license key for your %1$s plugin is expiring soon. 
			If you wish you can <a href="%2$scheckout/?edd_license_key=%3$s&utm_source=client-admin&utm_medium=renewal_link&utm_campaign=license_renewal">
			click here</a> to renew your license key directly so you can be sure your product feeds keep working.', 'wp-product-feed-manager' ), 
			$plugin_name[ 'Name' ], 
			WPPFM_EDD_SL_STORE_URL, 
			$lic_key ) : '';
		$msg_id = 'wppfm-licence-almost-expired';
		$msg_type = 'info';
	} else {
		update_option( 'wppfm_license_notice_surpressed', false );
	}

	if( $msg ) { 
		delete_option( 'wppfm_lic_status_date' ); // keep checking if there is an update on the license status
		
		?><div data-dismissible="notice-one-forever" class="notice notice-<?php echo $msg_type; ?> <?php echo $is_dismissible; ?>" id="<?php echo $msg_id; ?>">
			<p><?php echo $msg; ?></p>
		</div><?php
	}
}

add_action( 'admin_notices', 'wppfm_check_license_expiration' );

function wppfm_check_license( $license ) {
	// return false if no license is given
	if ( !$license ) { return false; }
	
	$item_name = urlencode( WPPFM_EDD_SL_ITEM_NAME );
	$api_params = array(
		'edd_action' => 'check_license',
		'license' => $license,
		'item_name' => $item_name
	);

	$response = wp_remote_get( add_query_arg( $api_params, WPPFM_EDD_SL_STORE_URL ), array( 'timeout' => 5, 'sslverify' => false ) );
	
	if ( is_wp_error( $response ) ) {
		/*translators: %s: link to the support page */
		echo wppfm_handle_wp_errors_response( $response, sprintf( esc_html__( '2122 - Checking your license failed. Please open a support ticket at %s for support on this issue.', 'wp-product-feed-manager' ), WPPFM_SUPPORT_PAGE_URL ) );
		return false; 
	}
	
	$license_data = json_decode( wp_remote_retrieve_body( $response ) );
	
	update_option( 'wppfm_lic_expires', $license_data->expires );
	
	if( $license_data && $license_data->license == 'valid' ) {
		return 'valid'; // this license is still valid
	} else {
		delete_option( 'wppfm_check_license_expiration' ); // reset the expiration messages
		return $license_data->license; // this license is no longer valid
	}
}